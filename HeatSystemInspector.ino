#include "OneWire.h"
#define ERROR_LED PA5
#define BUZZER PA6
#define ACK_BTN PC13
#define OW1_PIN PB13
#define OW2_PIN PA10
#define MAX_CMD_LEN 255
#define MAX_ANS_LEN 255
#define MAX_MILLIS_WAIT_CHAR 5000

OneWire ow1(OW1_PIN), ow2(OW2_PIN);
bool errorLed = false;
bool buzzer = false;
bool ackBtn = false;
bool searchOW1 = false, searchOW2 = false;

void listenBtnEdge();
void listenCmd();
void limitCharWait(char* rcommand, size_t& n);
bool parseCmd(char* rcommand, bool terminated);
void searchOneWire(bool &trig, OneWire &ow);
String formatOneWireAdr(byte rawAdr[]);

void setup() {
  // put your setup code here, to run once:
  pinMode(ERROR_LED, OUTPUT);
  pinMode(BUZZER, OUTPUT);
  pinMode(ACK_BTN, INPUT);
  Serial.begin(9600);
}

void loop() {
  // put your main code here, to run repeatedly:
  ackBtn = !digitalRead(ACK_BTN);
  listenBtnEdge();
  searchOneWire(searchOW1, ow1);
  searchOneWire(searchOW2, ow2);
  listenCmd();
  digitalWrite(ERROR_LED, !errorLed);
  digitalWrite(BUZZER, !buzzer);
}

void listenBtnEdge() {
  static bool last = false;
  if (ackBtn && !last)
    Serial.write("Button pressed\n");
  if (!ackBtn && last)
    Serial.write("Button released\n");
  last = ackBtn;
}

void listenCmd() {
  static char rcommand[MAX_CMD_LEN+1];
  static size_t n = 0;
  if (size_t ser_avl = Serial.available()) {
    size_t mem_avl = MAX_CMD_LEN - n;
    size_t read = mem_avl < ser_avl ?
                    Serial.readBytesUntil('\n', rcommand + n, mem_avl):
                    Serial.readBytesUntil('\n', rcommand + n, ser_avl);
    bool terminated = read < ser_avl;
    n += read;
    if (parseCmd(rcommand, terminated) || terminated) {
      memset(rcommand, '\0', MAX_CMD_LEN);
      n = 0;
    }
  }
  limitCharWait(rcommand, n);
}

void limitCharWait(char* rcommand, size_t& n) {
  static unsigned long last = 0;
  static unsigned long tick = 0;
  static unsigned long tock = 0;
  tock = millis();
  tick = (n == last) ? tock : tick;
  if (tock - tick >= MAX_MILLIS_WAIT_CHAR) {
    memset(rcommand, '\0', MAX_CMD_LEN);
    n = 0;
    last = 0;
  }
}

bool parseCmd(char* rcommand, bool terminated) {
  String scommand(rcommand);
  scommand.trim();
  scommand.toLowerCase();
  if (scommand == "error-on") {
    errorLed = true;
    Serial.println("error: on");
    return true;
  }
  if (scommand == "error-off") {
    errorLed = false;
    Serial.println("error: off");
    return true;
  }
  if (scommand == "buzzer-on") {
    buzzer = true;
    Serial.println("buzzer: on");
    return true;
  }
  if (scommand == "buzzer-off") {
    buzzer = false;
    Serial.println("buzzer: off");
    return true;
  }
  if (scommand == "ow1-search") {
    searchOW1 = true;
    return true;
  }
  if (scommand == "ow2-search") {
    searchOW2 = true;
    return true;
  }
  if (scommand.length() > 0 && terminated) {
    Serial.print("Unable to interpret: ");
    Serial.println(scommand);
  }
  return false;
}

void searchOneWire(bool &trig, OneWire &bus) {
  static OneWire *busBusy = NULL;
  static int count = 0;
  if (trig && !busBusy) {
    Serial.println(&bus == &ow1 ? "Searching OW1..." : &bus == &ow2 ? "Searching OW2..." : "Searching one-wire...");
    busBusy = &bus;
    count = 0;
    busBusy->reset_search();
    busBusy->target_search(0x28);
    return;
  }
  if (busBusy == &bus) {
    byte deviceAdr[8];
    if (busBusy->search(deviceAdr)) {
      Serial.println(String("\t" + formatOneWireAdr(deviceAdr)));
      ++count;
    }
    else {
      Serial.print(count);
      Serial.println(" devices found");
      //busBusy->reset_search();
      busBusy = NULL;
      trig = false;
    }
  }
}

String formatOneWireAdr(byte rawAdr[]) {
  String adrFmtd("");
  for (int i=0; i<8; ++i) {
    adrFmtd += String(i == 0 ? "" : ":") + String(rawAdr[i], HEX);
  }
  return adrFmtd;
}

bool parseHexByte(const String &sval, byte &value) {
  char chdigit;
  byte numdigit;
  if (sval.length()==0)
    return false;
  chdigit = sval.charAt(0);
  if (!parseHexHalfByte(chdigit, numdigit)
    return false;
  value = numdigit;
  if (sfam.length()>1) {
    chdigit = sfam.charAt(1);
    if (!parseHexHalfByte(chdigit, numdigit)
      return false;
    value *= 16;
    value += numdigit;
  }
  return true;
}

bool parseHexHalfByte(char raw, byte &num) {
  if (48 <= raw < 58)
    ascii = (byte)raw - 48;
  else if (65 <= raw < 70)
    ascii = (byte)raw - 55;
  else if (97 <= raw < 103)
    ascii = (byte)raw - 87;
  else return false;
  return true;
}
